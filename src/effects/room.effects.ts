import { Injectable } from '@angular/core';  
import { Effect, toPayload, Actions } from '@ngrx/effects';  
import { Observable } from 'rxjs/rx';

import { PouchService } from '../providers/PouchService';  
import { Rooms } from '../models/rooms.model';  
import { RoomActions } from '../actions/room.actions';

@Injectable()
export class BirthdayEffects {

    constructor(
        private actions$: Actions,
        private db: PouchService,
        private roomActions: RoomActions
    ) { }

    @Effect() addBirthday$ = this.actions$
        .ofType(RoomActions.ADD_ROOM)
        .map<Rooms>(toPayload)
       // .mergeMap(room => this.db.add(room));

    @Effect() updateBirthday$ = this.actions$
        .ofType(RoomActions.UPDATE_ROOM)
        .map<Rooms>(toPayload)
      //  .mergeMap(birthday => this.db.update(birthday));

    @Effect() deleteBirthday$ = this.actions$
        .ofType(RoomActions.DELETE_ROOM)
        .map<Rooms>(toPayload)
       // .mergeMap(birthday => this.db.delete(birthday));

       
    allRooms$ = this.db.getAll()  
            .map(rooms => this.roomActions.loadRoomsSuccess(rooms));

    /*changedBirthdays$ = this.db.getChanges()  
            .map(change => {
                if (change._deleted) {
                    return this.birthdayActions.deleteBirthdaySuccess(change._id);
                } 
                    else {
                        return this.birthdayActions.addUpdateBirthdaySuccess(change);
                    }
                });

    @Effect() getBirthdays$ = Observable.concat(this.allBirthdays$, this.changedBirthdays$); */
}