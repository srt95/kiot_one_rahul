import {ActionReducer, Action} from '@ngrx/store';  
import {RoomActions} from '../actions/room.actions';
import { Rooms } from '../models/rooms.model';

let nextId = 0;

export const RoomsReducer:ActionReducer<Rooms[]> = (state: Rooms[] = [], action: Action) => {  
    switch(action.type) {
        case RoomActions.ADD_ROOM:
            return [...state, Object.assign({}, action.payload, { id: nextId++ })];
        case RoomActions.UPDATE_ROOM:
            return state.map(room => {
                return room._id === action.payload.id ? Object.assign({}, room, action.payload) : room;
            });
        case RoomActions.DELETE_ROOM:
            return state.filter(room => room._id !== action.payload.id);

        case RoomActions.LOAD_ROOMS_SUCCESS:
            return action.payload;

        case RoomActions.ADD_UPDATE_ROOM_SUCCESS:
            var exists = state.find(room => room._id === action.payload._id);

            if (exists) {
                // UPDATE
                return state.map(room => {
                    return room._id === action.payload._id ? Object.assign({}, room, action.payload) : room;
                });
            } 
            else {
                // ADD
                return [...state, Object.assign({}, action.payload)];
            }
        case RoomActions.DELETE_ROOM_SUCCESS:
            return state.filter(room => room._id !== action.payload);
        default:
            return state;
    };
}