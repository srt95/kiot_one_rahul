import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar, Splashscreen, SQLite } from 'ionic-native';
import { NgZone } from "@angular/core";
import { ModalController } from 'ionic-angular';
import { HttpServ } from '../providers/HttpServ';
import * as PouchDB from 'pouchdb';
import { PouchService } from '../providers/PouchService';
import { TabsPage } from '../pages/tabs/tabs.page';
import { SliderPage } from '../pages/Start/slider/slider.page';
import { LocalStorageService } from '../providers/LocalStorageService';


@Component({
    template: `<ion-nav [root]="rootPage"></ion-nav>`,
    providers: []
})

export class MyApp {
    rootPage: any;
    _db;
    constructor(platform: Platform,private ps:PouchService,private ht:HttpServ,private local:LocalStorageService) {

       //this.ht.testInternet().subscribe(()=>localStorage.setItem("kiot_internet","online"))
       this.ht.testInternet();
        platform.ready().then(() => {

            if (this.local.getToken() == "" || this.local.getToken() == "undefined" || this.local.getToken() == "null" || this.local.getToken() == null) this.rootPage = SliderPage;
            else this.rootPage = TabsPage;
            //StatusBar.styleDefault();
            StatusBar.overlaysWebView(true);
            StatusBar.backgroundColorByHexString('#313131');
            Splashscreen.hide();


        });
        
        window["PouchDB"] = PouchDB;

    }


}


/*
            let db = new SQLite();
            db.openDatabase({
                name: "data.db",
                location: "default"
            }).then(() => {
                db.executeSql("CREATE TABLE IF NOT EXISTS HomeDataTable (id INTEGER PRIMARY KEY AUTOINCREMENT, TagName TEXT, HomeData TEXT)", {}).then((data) => {
                    console.log("TABLE CREATED: ", data);
                }, (error) => {
                    console.error("Unable to execute sql", error);
                })
            }, (error) => {
                console.error("Unable to open database", error);
            });
*/