import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';

//pages
import { EditIrAppPage } from '../edit-ir-app/edit-ir-app';
import { ConfigureRemotePage } from '../configure-remote/configure-remote';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';

@Component({
  selector: 'page-ir-app-details',
  templateUrl: 'ir-app-details.html'
})
export class IrAppDetailsPage {
appliance;name;type;brand;room_id;home_name;paired_sb;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService,private alert:AlertService,private ev:Events) {
    this.appliance = this.navParams.get('appliance');
  }

  ionViewWillEnter() {
    console.log(this.appliance);
      this.name= this.appliance.name;
      this.type = this.appliance.type;
      this.brand=this.appliance.brand;
      this.room_id=this.appliance.room_id.name;
      this.paired_sb = this.appliance.paired_sb.sboard_name;
      this.home_name = this.navParams.get('home').display_name;
      console.log(this.appliance._id);
  }
  goToEditIrApp(){
        this.navCtrl.push(EditIrAppPage,{
          appliance:this.appliance,
          home_id:this.navParams.get('home')._id
        })
  }

  deleteIr(){
    this.alert.showConfirm("Are you Sure?","this deletes IR Appliance and corresponding remote","No","Delete",
    ()=>{
        console.log('cancel!!');
    },()=>{
      let obj={
      "id":this.appliance._id
    };
        this.ds.postDeleteIrApp(obj,
        (data)=>{
              console.log(data);
              this.navCtrl.pop();
        },
        (error)=>{
              console.log(error);
        });
  
    })
  }
  goToConfigureRemote(){
      this.navCtrl.push(ConfigureRemotePage,{
        _id:this.appliance._id,
          name:this.appliance.name,
          room:this.appliance.room_id._id,
          sb:this.appliance.paired_sb._id,
          device:this.appliance.paired_sb.device_id,
         // device:this.sb_id.device_id,
          home_id:this.navParams.get('home')._id,
          type:this.type,
        mode:"edit",
        codeset:this.appliance.remote_codeset,
        brand:this.appliance.brand
      })
  }  


}
