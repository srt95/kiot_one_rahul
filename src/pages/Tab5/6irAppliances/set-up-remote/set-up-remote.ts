import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { LoadingService } from '../../../../providers/LoadingService';

@Component({
  selector: 'page-set-up-remote',
  templateUrl: 'set-up-remote.html'
})
export class SetUpRemotePage {
data;
brand;
type;
number_of_remotes;
row1_keys=[];
row2_keys=[];
keys=[];
present_remote;
waiting;
disable_next;
disable_prev;
moreThanOne;
codeset=[];
switchboard_id;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService,private load:LoadingService) {
        this.brand = this.navParams.get('brand');
        this.type=this.navParams.get('type');
        this.waiting=false;
        this.disable_next=false;
        this.disable_prev =true;
        this.moreThanOne = false;
        this.codeset = this.navParams.get('codeset');
        this.switchboard_id = this.navParams.get('sb');
  }

  ionViewDidLoad() {
    this.ds.getTestCodeSets(this.brand,this.type,
      (data)=>{
            console.log(data);
            this.data = data;
            this.number_of_remotes = this.data.length;
            if(this.number_of_remotes > 1) this.moreThanOne = true;
            this.present_remote = 1;
            this.remoteConfig();
       },
       (error)=>{

       });
  }
  remoteConfig(){
        this.keys = Object.keys(this.data[this.present_remote - 1].keys[0]);
        this.row1_keys = this.keys.slice(0,3);
        this.row2_keys = this.keys.slice(3,5);
        console.log(this.row1_keys);
        console.log(this.row2_keys);
        console.log(this.keys);

  }
  triggerIr(key){
  this.waiting =true;
  let keyHere  =  this.data[this.present_remote-1].keys[0];
  let len;
  let obj;
  let protocol;let proto_name;let wave_gap;
    switch(this.data[this.present_remote-1].type){
      case 1:{
                len = this.data[this.present_remote-1].len;
                protocol = this.data[this.present_remote-1].protocol;
                proto_name = this.data[this.present_remote-1].proto_name;
                wave_gap =this.data[this.present_remote-1].wave_gap;
                obj={
                  "device":this.navParams.get('device'),
                  "code": keyHere[key],
                  "len":len,
                  "proto_name":proto_name,
                  "protocol":protocol,
                  "wave_gap":wave_gap,
                  "switchboard": this.switchboard_id
                };
                break;
      }
      case 2:{

                 len = parseInt(keyHere[key].first_seq_len) + parseInt(keyHere[key].sec_seq_len);
                 obj={
                  "device": this.navParams.get('device'),
                  "freq" : this.data[this.present_remote-1].freq,
                  "code": keyHere[key].code,
                  "len": len,
                  "first_seq_len": keyHere[key].first_seq_len,
                  "sec_seq_len": keyHere[key].sec_seq_len,
                  "rpt_cnt": this.data[this.present_remote-1].rpt_cnt,
                  "switchboard":this.switchboard_id

                };
                break;
      }
    }
   // console.log(obj);
      this.ds.postTriggerIr(obj,
        (data)=>{
            console.log(data);
            this.waiting = false;
        },
        (error)=>{
            console.log(error);
            this.waiting = false;
        });
  }
  nextRemote(){
    this.present_remote++;
    this.load.showTimeOutLoading(80);
    if(this.present_remote<this.number_of_remotes){
      this.disable_prev = false;
    }
    else{
        this.disable_next = true;
    }
    
  }

  prevRemote(){
  this.present_remote--;
  this.load.showTimeOutLoading(80);
    if(this.present_remote>1){
        this.disable_next = false;
    }
    else {
        this.disable_prev = true;
    }
}
setRemote(){
    this.codeset.splice(0,2);
    this.codeset.push(this.data[this.present_remote - 1]._id);
    this.codeset.push(this.data[this.present_remote - 1].name);
    //this.codeset[0] = this.data[this.present_remote - 1].name;
    console.log(this.codeset);
    this.navCtrl.pop();
}
}
