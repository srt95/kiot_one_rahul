import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';

@Component({
  selector: 'page-edit-ir-app',
  templateUrl: 'edit-ir-app.html'
})
export class EditIrAppPage {
items;room;sb;disSb;appliance;name;sb_id;room_name;paired_sb_name;disUpdate;
sboardIdToPost;sboardNameToSave;

  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService) {}

  ionViewDidLoad() {
    this.disUpdate = true;
    //console.log('ionViewDidLoad EditIrAppPage');
    this.appliance = this.navParams.get('appliance');
    this.name = this.appliance.name;
         this.ds.getSboards(this.navParams.get('home_id'),
          (data)=>{
                  console.log(data);
                  this.items = data;
                  for(let i of this.items){
                        if(i.room_id == this.appliance.room_id._id){
                          i.selected = true;
                          console.log(i);
                          this.room = i.room_id;
                          this.room_name = i.room_name;
                          this.sb = i.devices;
                          for(let j of this.sb){
                            if(j._id == this.appliance.paired_sb._id){
                              j.selected = true;
                              this.sboardIdToPost = j._id;
                              this.sboardNameToSave = j.name;
                              //this.sb_id._id = j._id;
                            }
                            else j.selected=false;
                          }
                        }
                        else i.selected = false;
                  }
        },
           (error)=>{
             console.log(error);
            console.log(JSON.stringify(error.json()));

        });
  }
  onChangeSb(){
    this.disUpdate = false;
    this.sboardIdToPost = this.sb_id._id;
    this.sboardNameToSave = this.sb_id.name;
  }
  onChange(){
    this.disUpdate = false;
  }
    onRoomChange(){
        console.log(this.room);
        this.disUpdate = false;
        for(let i of this.items){
        if(i.room_id ==this.room){
              this.sb  = i.devices;
              console.log(this.sb);
              this.room_name = i.room_name;
        }
        }
        this.disSb = false;
  }
  updateIrApp(){
   // this.onRoomChange();
    let obj = {
          "id":this.appliance._id,
          "name":this.name,
          "room_id":this.room,
          "paired_sb":this.sboardIdToPost
    };
    this.ds.postEditIrApp(obj,
        (data)=>{
              console.log(data);
              this.appliance.name = this.name;
              this.appliance.room_id.name=this.room_name;
            /*  for(let k of this.sb){
                if(k._id == this.sb._id)
                    this.paired_sb_name = k.name;
              }*/
              this.appliance.paired_sb.sboard_name = this.sboardNameToSave; 
              this.navCtrl.pop();

        },
        (error)=>{

    });
  }

}
