import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';

import { roomModal } from '../room.modal';

@Component({
  selector: 'page-edit-room',
  templateUrl: 'edit-room.html'
})
export class EditRoomPage {
room;name;type;disUpdate;
public roomModal:roomModal;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService) {
  		this.roomModal=this.room  =  this.navParams.get('room');
      this.disUpdate = true;
  		//this.name = this.room.name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditRoomPage');

  }
  onChange(){
    this.disUpdate = false;
  }
  editRoom(){
  	let updatedRoomData = {
		  "room": 	this.roomModal._id,
		  "name": 	this.roomModal.name,
		  "room_type": this.roomModal.room_type
  	};
    this.ds.postEditRoom(updatedRoomData,data => {
                console.log(data+"Successfully Edited");
               // this.toaster("Edit Successful");
                this.navCtrl.pop();
              },
              error =>{
                 // this.toaster("error, cannot edit home");
              });
  	}





  }


