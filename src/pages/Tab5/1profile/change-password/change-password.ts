import { Component } from '@angular/core';
import { AlertController,NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { AlertService } from '../../../../providers/AlertService';


@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html'
})
export class ChangePasswordPage {
new1;new2;old;otp;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService,public alertCtrl: AlertController,private alert:AlertService) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }
  changePassword(){
    if(this.new1!=this.new2){
        //toastHere
        this.alert.showAlert("Passwords do not match","please enter the same new password in both fields");
    }
    else{
          let obj={
            "old_password": this.old,
            "new_password": this.new1
          }
          this.ds.postUpdatePassword(obj,
          data => {
              console.log(data);
            //toastHere
            if(data.status == 0)
            this.alert.showAlert("Error",data.err);
            else
              this.navCtrl.pop();

        }, error => {
            //toastHere
          console.log(error);
            console.log(JSON.stringify(error.json()));
        });
    }
  }
  getOtpPassword(){
    let alert = this.alertCtrl.create({
    title: 'Enter OTP',
    inputs: [
     
      {
        name: 'otp',
        placeholder: 'Enter OTP',
        type: 'otp'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Submit',
        handler: data => {
          
        }
      }
    ]
  });
  alert.present();
}
}
