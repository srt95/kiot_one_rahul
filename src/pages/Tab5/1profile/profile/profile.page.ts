import { Component } from '@angular/core';
import { NavController, NavParams,App,AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

//pages
import { LoginPage } from '../../../Start/login/login';
import { RegisterHomePage } from '../../2homes/registerhome/registerhome';
import { DisplayProfilePage } from '../display-profile/display-profile';
import { HomeListPage } from '../../4sboards/home-list/home-list';
import { DisplayRoomsPage } from '../../3rooms/display-rooms/display-rooms';
import { SboardListPage } from '../../4sboards/sboard-list/sboard-list';




@Component({
  selector: 'page-profile',
  templateUrl: 'profile.page.html'
})
export class ProfilePage {
feedback;
  constructor(public navCtrl: NavController,public modalCtrl: ModalController, public navParams: NavParams,private app:App,private alertCtrl:AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  
  logout(){
    let alert = this.alertCtrl.create({
    title: 'Confirm Logout',
    message: 'Do you want to logout?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
            localStorage.setItem("token","");
            console.log(localStorage.getItem("token"));
            //window.location.reload();
            this.app.getRootNav().setRoot(LoginPage);
            //console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();
  }
  goTomyHomes(){
      this.navCtrl.push(RegisterHomePage,{
          show: "1"
      });
  }
  goTomyHomesForRooms(){
      this.navCtrl.push(DisplayRoomsPage,{
          show: "0"
      });
  }
  goToDisplayProfile(){
    this.navCtrl.push(DisplayProfilePage);
  }
  goTomyHomesForSboards(){
    this.navCtrl.push(HomeListPage,{
      for:"sboards"
    });
  }
  goTomyHomesForSwitches(){
    this.navCtrl.push(HomeListPage,{
      for:"switches"
    });
  }
  goToHomeListForIR(){
    this.navCtrl.push(HomeListPage,{
      for:"ir"
    })
  }
sendFeedback(){
  let alert = this.alertCtrl.create({
    title: 'Compose Feedback',
    subTitle: 'Hello, Nice to Hear from you!',
    inputs: [
     
      {
        name: 'feedback',
        placeholder: 'e.g. Remote not working',
        type: 'feedback'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
            subTitle: 'Hello, Nice to Hear from you!',

        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Send',
            subTitle: 'Hello, Nice to Hear from you!',

        handler: data => {
          
        }
      }
    ]
  });
  alert.present();
}

  feedbackalert(){
    let alert = this.alertCtrl.create({
    title: 'Compose Feedback',
      message: 'Hi, thanks for feedback',


    inputs: [
     
      {
        name: 'feedback',
        placeholder: 'e.g. Remote is not working',
        type: 'Feedback',
        
      }
    ],
    buttons: [
      {
        text: 'Send',
        handler: data => {
          
        }
      },
      {
        text: 'Email Us',
        message: 'e.g. Remote is not working',

        role: 'contact',
        handler: data => {
        console.log('email clicked');
        }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      }
      
    ]
  });
  alert.present();
}
  }

        


