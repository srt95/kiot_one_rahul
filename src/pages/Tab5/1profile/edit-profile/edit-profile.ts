import { MaxLengthValidator, MinLengthValidator } from '@angular/forms/src/directives';
import { Component } from '@angular/core';
import { AlertController,NavController,NavParams,Platform } from 'ionic-angular';


//providers
import { DataService } from '../../../../providers/DataService';
import { LocalStorageService } from '../../../../providers/LocalStorageService';
import { AlertService } from '../../../../providers/AlertService';

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {
name;phone;gender;def;items;data;obj;defDisable;disUpdate;otp;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService,private local:LocalStorageService,public alertCtrl: AlertController,private alert:AlertService) {
      
       this.obj = JSON.parse(this.local.getUser());
       this.name=this.obj.name;
        this.phone=this.obj.phone;
        this.def = this.obj.default_home;
        this.gender = this.obj.gender;
        this.defDisable=false;
           
           this.disUpdate=true;
     /*   for(let i of this.items){
          if(i._id ==  this.def) i.selected=true;
          else i.selected=false;
          }*/
  } 

  ionViewDidLoad() {

  }
  updateProfile(){
    /*let obj = {
        "default_home": this.def,
        "name": this.name,
        "gender": this.gender
    };*/
    
    this.obj.default_home=this.def;
    this.obj.name =this.name;
    this.obj.phone =this.phone;
    let re = new RegExp(/^[0-9]*$/);
        if(re.test(this.phone) && this.phone.length ==10 && this.name.length >= 3) {
            
             this.obj.gender=this.gender;
    this.ds.putEditProfile(this.obj,data => {
          console.log(data);
          //toastHere
          this.navCtrl.pop();
          this.local.setUser(JSON.stringify(this.obj));
          this.local.setDefaultHome(this.def);
        }, error => {
            console.log(error);
            console.log(JSON.stringify(error.json()));
        });
            
        }
        else {
            console.log('Invalid');
                this.alert.showAlert("Error","Check Details");
        }
        
   
    /*this.ht.getMe('users/me')   
        .subscribe(data => {
          console.log(data);
          this.data = data;
          this.data = JSON.parse(this.data._body);
          localStorage.setItem('user',JSON.stringify(this.data));
          //toastHere
        }, error => {
            console.log(error);
            console.log(JSON.stringify(error.json()));
        });*/
        
  }
  onChange(){
    this.disUpdate=false;
}
getOtpPassword(){
    let alert = this.alertCtrl.create({
    title: 'Enter OTP',
    inputs: [
     
      {
        name: 'otp',
        placeholder: 'Enter OTP',
        type: 'otp'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Submit',
        handler: data => {
          
        }
      }
    ]
  });
  alert.present();
}
}
