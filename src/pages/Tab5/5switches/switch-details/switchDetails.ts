import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../../providers/DataService';
import { Helper } from '../../../../providers/Helper';
import { AlertService } from '../../../../providers/AlertService';

//pages
import { CreateSwitchPage } from '../create-switch/create-switch';

@Component({
  selector: 'page-switch-details',
  templateUrl: 'switchDetails.html',
  providers: []
})
export class SwitchDetailsPage  {
  sboard_id; items;
  public homeData: any;
  public switchDetailsData: any;
  public homeName: string;
  public sbNmae: string;
  public rmName: string;
data;data2;disUpdate;appliance_subtype_exists;appliance_type_exists;name;
appliance_type;sub_appliance_type;appliance_changed;sub_appliance_changed;
  constructor(public navCtrl: NavController,private alert:AlertService, public navParams: NavParams, private ds: DataService,private help:Helper) {
   this.switchDetailsData = this.navParams.get('editSwitch');
    this.homeName = this.navParams.get('homeName');
    this.sbNmae = this.navParams.get('sbNmae');
    this.rmName = this.navParams.get('rmName');
    this.name =  this.switchDetailsData.name;
    (this.switchDetailsData.appliance_type)?this.appliance_type_exists =true:this.appliance_type_exists=false;
    (this.switchDetailsData.appliance_subtype)?this.appliance_subtype_exists =true:this.appliance_subtype_exists=false;
    this.appliance_changed = false;this.sub_appliance_changed = false;
   // console.log(this.appliance_type);
  }

ionViewWillEnter(){
  console.log(this.switchDetailsData);
  this.ds.getGeneralAppliances(
    (data)=>{
        this.data=data;
        for(let x of this.data){
          if(x.appname==this.switchDetailsData.appliance_type){
            x.selected  =true;
            this.data2 =x.subtypes;
            for(let y of this.data2){
              (y.name==this.switchDetailsData.appliance_subtype)?y.selected=true:y.selected=false;
            }
          }
          else{
            x.selected=false;
          }
        }
        console.log(this.data);
    },
    (error)=>{

    }
  )
 // if(this.appliance_subtype_exists)this.subType();
}
isEmpty(){
  console.log(this.name);
  if(this.help.isEmpty(this.name) || !this.appliance_type_exists || !this.appliance_subtype_exists){
     console.log('im here insidee');
    return true;
  }
  else return false;
}

  public addSwitch() {
    this.navCtrl.push(CreateSwitchPage, {
      sboard_id: this.sboard_id
    });
  }
  subType(){
    console.log(this.appliance_type);
    this.appliance_type_exists=true;
    this.appliance_subtype_exists=true;
    this.appliance_changed = true;
    this.data2 =this.appliance_type.subtypes;
  }
  public updateSwitch() {
    if(this.isEmpty()){
      this.alert.showAlert("Please fill all fields","Cannot configure unless all the fields are filled");
    }
    else{
      console.log(this.sub_appliance_type);
      this.switchDetailsData.name = this.name;
     if(this.appliance_type) this.switchDetailsData.appliance_type=this.appliance_type.appname;
     if(this.sub_appliance_type) this.switchDetailsData.sub_appliance_type = this.sub_appliance_type.name;
    let obj = {
      "id": this.switchDetailsData._id,
      "name": this.switchDetailsData.name,
      "sboard_id": this.switchDetailsData.sboard_id,
      "appliance_type": this.switchDetailsData.appliance_type,
      "appliance_subtype": this.switchDetailsData.sub_appliance_type,
       "is_configured":true,
       "active_wattage":""
    }
    if(this.sub_appliance_type)obj.active_wattage=this.sub_appliance_type.wattage;


    this.ds.postEditSwitch(obj,
    data => {      
      this.navCtrl.pop();
    }, error => {
      console.log(JSON.stringify(error.json()));
    });
    }
  }



}
