export class switchModal {
    _id: string;
    switch_no: string;
    sboard_id: string;
    name: string;
    switch_type: string;
    appliance_type: any;
    home_type: string;
    room_type: string;
    sboard_type: string;
    is_configured:boolean;
    sub_appliance_type:any;


}
export class subtype{
    name:string;
    wattage:number;
}