import { Component, OnInit,NgZone } from '@angular/core';
import { NavController,App,AlertController } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { AlertService } from '../../../providers/AlertService';

//pages
import { SwitchesInRoomPage } from '../switches-in-room/switches-in-room';

//Models
import { getSboards } from '../../../models/getSboards'

@Component({
  selector: 'ib-page-createHome',
  templateUrl: 'rooms.page.html',
  providers : []
})
export class RoomsPage {
  data:getSboards[];
  def;
  switches=[];
  
  constructor(public navCtrl: NavController,private alert:AlertService,private zone:NgZone, private ds: DataService, public alertCtrl: AlertController,private app:App,private local:LocalStorageService) {

  }
ionViewWillEnter(){
  this.def = this.local.getCurrentHome();
  /*
this.zone.run(() => {
    this.ds.getSboards(this.def,
    data => {
          console.log(data);
          this.data = data;
          for(let x of data){
            x.switches = [];
            x.active_count = 0;
              let k=0;
              for(let i of x.devices){
                for(let j of i.switchappliances){
                  x.switches[k]=j;
                  x.switches[k].sb_id=i._id;
                  x.switches[k].device_id=i.device_id;
                  if(j.switch_type=='regulator') x.switches[k].isRegulator = true;
                  else x.switches[k].isRegulator = false;
                  if(j.current_status == 0){
                    x.switches[k].isActive = false;
                  //  console.log(x.switches[k].active_count);
                  }
                  else if(j.current_status == 1){
                    x.switches[k].isActive = true;
                    x.active_count++;
                  } 
                  k++;
                  
                }
              }
              console.log(x.active_count);
          }
          //console.log(this.items);
        }, error => {
            console.log(error);
            
    });
});*/
  this.ds.getRooms(this.def,
    (data)=>{
          this.data = data;
    },
    (error)=>{

    });

}
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 99);
  }
goToSwitchesinTheRoom(room){
      this.navCtrl.push(SwitchesInRoomPage,{
        room_id:room.room_id
      });
}
turnOffRoom(i){
  this.alert.showConfirm('Switch Off?',
  'this switches off all the appliances in '+i.room_name,'Cancel','Switch Off',()=>{},
  ()=>{
        this.ds.postTurnOffRoom(
      {
        "room":i.room_id
      },
      (data)=>{
          console.log(data);
          i.activeAppliances = 0;
      },
      (error)=>{
          console.log(error);
          this.alert.showAlert('Cannot Switch Off',error+'Please Try Again');
      });
  })

}
}