import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//pages
import { SwitchHistoryPage } from '../switch-history/switch-history';

@Component({
  selector: 'page-switch-stats',
  templateUrl: 'switch-stats.html'
})
export class SwitchStatsPage {
switch;name;durationHrs;durationMins;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.switch = this.navParams.get('switch');
    console.log(this.switch);
    this.name = this.switch.name;
    this.durationHrs = this.switch.durationHrs;
    this.durationMins = this.switch.durationMins;
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad SwitchStatsPage');

  }
  goToHistory(){
    this.navCtrl.push(SwitchHistoryPage,{
      switch:this.switch
    })
  }

}
