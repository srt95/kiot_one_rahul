import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-switch-history',
  templateUrl: 'switch-history.html'
})
export class SwitchHistoryPage {
switchData;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.switchData=this.navParams.get('switch');
    for(let i of this.switchData.switch_events){
      if(i.event_type!=0&&i.event_type!=1){
        i.showHistory = false;
      }
      else i.showHistory = true;
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SwitchHistoryPage');
  }

}
