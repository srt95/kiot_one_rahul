import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';
import { Helper } from '../../../providers/Helper';

//pages
import { SwitchStatsPage } from '../switch-stats/switch-stats';

@Component({
  selector: 'page-switches-in-room',
  templateUrl: 'switches-in-room.html'
})
export class SwitchesInRoomPage {
room_id;switches=[];switchShow;isActive;data;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService,private help:Helper) {      
  }
  ionViewWillEnter(){
          this.room_id = this.navParams.get('room_id');
    /*    let k=0;
        for(let i of this.room.devices){
          for(let j of i.switchappliances){
            this.switches[k]=j;
            this.switches[k].sb_id=i._id;
            this.switches[k].device_id=i.device_id;
            if(j.current_status == 0)
              this.switches[k].isActive = false;
            else if(j.current_status == 1)
              this.switches[k].isActive = true; 
            k++;
          }
        }*/
        let duration=[];
        this.ds.getSwitchesInRoom(this.room_id,
          (data)=>{
            console.log(data);
              this.data=data;
              for(let j of this.data){
                j.sorter = j.switch_events.length; 
           
                if(j.current_status == 1)
                  j.isActive = true;
                else if(j.current_status == 0)
                  j.isActive = false;
                
                if(j.switch_type == 'regulator') j.isRegulator = true;
                else {
                  j.isRegulator = false; 
                }                  
                  duration=this.help.findDuration(j.switch_events,j.current_status);
                  j.durationHrs =duration[0];
                  j.durationMins = duration[1];
                  j.progressBarDuration = duration[2];
                  if(j.progressBarDuration==0) j.isRounded = false;
                  else j.isRounded = true;
                  j.wattage = Math.floor((parseInt(j.active_wattage)*duration[0])+(j.active_wattage*(duration[1]/60)));
                  //console.log(j.duration);

                
              }
          },
          (error)=>{

          })
  }


  act;sw_no;on_off;
trigger(i){
  i.isActive = !i.isActive;
  this.sw_no = i.switch_no;
  if(i.isActive==false) this.on_off=0;
  else if(i.isActive==true) this.on_off=1; 
  console.log(i);
 // this.act = this.sw_no.toString()+this.on_off.toString();
 let time = Date.now();
  let obj={
        "device": i.device_id,
        "sb": i.sboard_id,
        "switch_no": this.sw_no.toString(),
        "switch_state": this.on_off,
        "key": time,
        "mobId": "98239023",
        "act_type": "switch",
        "id": (i._id).split("_")[2]
  };
  console.log(obj);
  this.ds.postTriggerSwitch(obj,
      (data)=>{
            console.log(data);
            if(data.status==1){
              console.log('yeahhh');
              let obj2={
                  timestamp:time,
                  event_type:this.on_off
              }
              i.switch_events.push(obj2);
            }
      },
      (error)=>{
            console.log(error);
      })

}
updateDimm(i){
   let obj={
        "device": i.sboard_id,
        "sb": i.sboard_id,
        "switch_no": i.switch_no.toString(),
        "key": Date.now(),
        "mobId": "98239023",
        "act_type": "regulate",
        "id": (i._id).split("_")[2],
        "dimm_value":i.current_dimm
  };
    console.log(obj);
  this.ds.postTriggerSwitch(obj,
      (data)=>{
            console.log(data);
      },
      (error)=>{
            console.log(error);
      })
}
roundProgressClick(switchData:any,roomDetails:any){
 
  for(let i=0;i<this.switches.length;i++){
    if(switchData._id==this.switches[i]._id){ 
      this.switchShow=switchData._id;
      this.switches[i].isActive=!this.switches[i].isActive;
      
    }
  }
 let obj={
  "device":roomDetails.device_id,
  "sb": switchData.sboard_id,
  "act": "21",
  "key": "12345",
  "mobId": "9015555",
  "act_type": "switch"
}

 /*this.ds.postDeviceevents(obj,
    data => {
          console.log(data);
        }, error => {
            console.log(error);            
    });*/




}
doRefresh(refresher) {
   // console.log('Begin async operation', refresher);
    this.ionViewWillEnter();
    setTimeout(() => {
     // console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
  }
goToStats(item){
  console.log(item);
    this.navCtrl.push(SwitchStatsPage,{
       switch:item
    });
}
  }