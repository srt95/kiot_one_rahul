import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { PouchService } from '../../../providers/PouchService';
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';

//pages
import { EpgMainPage } from '../epg-main/epg-main';

@Component({
  selector: 'page-real-remote',
  templateUrl: 'real-remote.html'
})
export class RealRemotePage {
codeset;remote_keys;data;remote_keys1;device_id;irSignal:any;is_stb;sb_id;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alert:AlertService,private ps:PouchService,private ds:DataService) {
    this.codeset = this.navParams.get('codeset');
    this.device_id = this.navParams.get('device_id');
    this.sb_id = this.navParams.get('sb_id');
    this.irSignal={
      "device":this.device_id,
      "switchboard":this.sb_id

    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RealRemotePage');
  }
  ionViewWillEnter(){

    
   /* this.ps.initDB('KIOT');
    this.ps.getADoc('codeset',this.codeset+"22").then((data)=>
     {
       console.log(data);
       this.data = data;
       this.remote_keys = this.data[0].keys[0];
       this.remote_keys1 = Object.keys(this.remote_keys);
       console.log(this.remote_keys1);
      });*/
      this.ds.getRemoteFromDb(this.navParams.get('codeset'),
      data=>{
        this.data=data;
            this.is_stb = this.data[0].is_stb;
        console.log(this.data);
        this.remote_keys = this.data[0].keys[0];
       this.remote_keys1 = Object.keys(this.remote_keys);
       console.log(this.remote_keys1);
      },error=>{
          console.log(error);
      });
  }
  triggerIr(i){
      console.log(this.remote_keys[i]);

      switch(this.data[0].type){
          case 1:{
            this.irSignal.protocol = this.data[0].protocol;
            this.irSignal.code = this.remote_keys[i];
            this.irSignal.len = this.data[0].len;
            this.irSignal.proto_name = this.data[0].proto_name;
            this.irSignal.wave_gap = this.data[0].wave_gap;
            break;                
          }
          case 2:{
                this.irSignal = this.remote_keys[i];
                this.irSignal.len = parseInt(this.remote_keys[i].first_seq_len)+parseInt(this.remote_keys[i].sec_seq_len);
                this.irSignal.freq = this.data[0].freq;
                this.irSignal.rpt_cnt = this.data[0].rpt_cnt;
                break;
          }
      }
      this.ds.postTriggerIr(this.irSignal,
        (data)=>{
            console.log(data);
        },
        (error)=>{
            console.log(error);
        })

  }
  goToEpgPage(){
      this.ds.getChannelGroupData(
        data=>{
          console.log("I think i am all done safely");
          this.alert.showAlert("Reached view Enter",JSON.stringify(data));
            this.navCtrl.push(EpgMainPage,{
              channelGroupData:data
            });
        },
        error=>{
          this.alert.showAlert("Reached view Enter",JSON.stringify(error));
          console.log("Opps mann.. check it");
              console.log(error);
        });
      
  }
}
