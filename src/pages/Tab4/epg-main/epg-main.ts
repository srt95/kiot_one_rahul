import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';

//providers
import { LocalStorageService } from '../../../providers/LocalStorageService';
import { DataService } from '../../../providers/DataService';
import { AlertService } from '../../../providers/AlertService';

//pages
import { ProgramDetailsPage } from '../program-details/program-details';

@Component({
  selector: 'page-epg-main',
  templateUrl: 'epg-main.html'
})
export class EpgMainPage {

show;channelMetaData;langs;prog_types;rowData=new Array();colData=[];types;rowCount;noMoreRows;
  constructor(public navCtrl: NavController,private navParams:NavParams,private alert:AlertService,private ds:DataService,private alertCtrl:AlertController,private local:LocalStorageService) {
      this.alert.showAlert("Reached Contructor","checkpoint");
      this.noMoreRows = false;
      this.show=true;
      this.types= [
        {
          "key":"TV-Series",
          "value":"TV Shows"
        },
        {
          "key":"Movie",
          "value":"Movies"
        },
        {
          "key":"Music",
          "value":"Music"
        },
        {
          "key":"News",
          "value":"News"
        },
        {
          "key":"Sports",
          "value":"Sports"
        },
        {
          "key":"Health",
          "value":"Health"
        },
        {
          "key":"Travel",
          "value":"Travel"
        },
        {
          "key":"Spiritual",
          "value":"Spiritual"
        },
        {
          "key":"Astrology",
          "value":"Astrology"
        }
      ];
      
  }

ionViewWillEnter(){
      this.rowData = [];this.colData=[];
      
      this.channelMetaData = this.navParams.get('channelGroupData');
      this.langs = this.local.getEpgLangs();
      if(this.local.getEpgTypes()===undefined || this.local.getEpgTypes()==null)
      {
        this.prog_types = this.types; 
        this.local.setEpgTypes(JSON.stringify(this.types));
        console.log("Bello");
        
      }
      else
      {
        this.prog_types = this.local.getEpgTypes();
      }
      console.log(this.local.getEpgTypes());
      console.log(this.types);
      console.log(this.prog_types);
      this.rowData = this.prog_types;
      
      if(!this.langs)this.doCheck();
       
      if(this.prog_types.length==1){
        this.alert.showAlert("Reached view Enter","checkpoint");
          this.rowCount=2;
          this.getARow(this.prog_types[0].key,1,this.langs);
      }
      else{
        this.alert.showAlert("Reached view Enter","checkpoint");
      this.rowCount = 4;
      this.getARow(this.prog_types[0].key,3,this.langs);
      this.getARow(this.prog_types[1].key,3,this.langs);
      this.getARow(this.prog_types[2].key,3,this.langs);
      }


}
getARow(rows,num,lang){
      this.ds.getPrograms(10,0,lang,rows,false,
          (data)=>{
           // console.log(data);
            for(let i of data){
              let pType = num==-1?lang+i.program.type:i.program.type;
               i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
              if(!this.colData[pType]) this.colData[pType]=[];
               this.colData[pType].push(i);
            }
               if(num==1){  //for single category like Music,Movies,News etc

                    let c=1;
                    let currentType = this.prog_types[0].key.toString();
                    let langsArray = [];
                    langsArray = this.langs.toString().split(',');
                    console.log(langsArray);
                    if(langsArray.length>1){
                    for(let k of langsArray){
                      let x = k.toString(); let y=this.prog_types[0];
                      if(this.colData[k]!=[]){this.rowData.push({"key":x+y.key,"value":x+" "+y.value});this.rowCount++}
                      else this.colData[k]=[];
                       this.getARow(this.prog_types[0].key,-1,k); //-1 is sent for "combo" - Telugu Movies,English Music etc
                      /*
                      for(let j of this.colData[currentType]){
                        if(j.program.language == k){
                          console.log(c++);
                          this.colData[k]?{}:this.colData[k]=[];
                          this.colData[k].push(j);
                        }
                      }*/

                    }
                    
                    }
               } //if num==1 ends here

            
            console.log(this.rowData);
            console.log(this.colData);

          },
          (err)=>
          {
            console.log(err);
          });
}

public doCheck() {
  
    let alert = this.alertCtrl.create();
    let alert2 = this.alertCtrl.create();
   // alert = this.alertCtrl.create();
    alert.setTitle('Select Language(s)');

     let langList = this.channelMetaData[0].languages;
    console.log(this.channelMetaData);
    for(let i of langList){
             alert.addInput({
                     type: 'checkbox', 
                     label: i, 
                     value: i 
              });
    }
    alert.addButton({
      text:'Cancel',
      handler : data=>{
        this.local.setEpgLangs(langList.toString());
        this.ionViewWillEnter();
      }
    });
    alert.addButton({
         text: 'Ok',
         handler: data => {
                 if(data){ alert.dismiss();}
                   this.local.setEpgLangs(data);
                   this.ionViewWillEnter();
                  
                }
              });
       alert.present();


  }
selectType() {
  
    let alert = this.alertCtrl.create();
   // alert = this.alertCtrl.create();
    alert.setTitle('Select a Category');
     let UniqueNames = [];
      this.types.forEach(function(value) {
      if (UniqueNames.indexOf(value.value) === -1) {
          UniqueNames.push(value.value);
      }
    });
    alert.addInput({
      type:'radio',
      label:'All Categories',
      value:this.types
    })
    for(let i of UniqueNames){
             alert.addInput({
                     type: 'radio', 
                     label: i, 
                     value: this.findValue(i)
              });
    }
    alert.addButton('Cancel');
    alert.addButton({
         text: 'Ok',
         handler: data => {
                 if(data){ alert.dismiss(); }
                   this.local.setEpgTypes(JSON.stringify(data));
                  this.noMoreRows=false;
                   this.ionViewWillEnter();
                }
              });
       alert.present();


  }
findValue(i):any{
  let x=[];
  for(let y of this.types){
    if(y.value == i) x.push(y);
  }
    return x;
}
doInfinite(event){
  //console.log('yoyoyoyoyo');
if(this.rowCount<=this.prog_types.length && this.rowCount>1){
  setTimeout(() => {
       this.ds.getPrograms(10,0,this.langs,this.prog_types[this.rowCount-1].key,false,
          (data)=>{
           event.complete();
           // console.log(data);
            for(let i of data){
              i.channel_name = this.channelMetaData[3].channels[i.channelid].channelname;
              console.log(i.channel_name);
              if(!this.colData[i.program.type]) this.colData[i.program.type]=[];
               this.colData[i.program.type].push(i);
            }
            
             this.rowCount++;
              console.log(this.prog_types);
              //this.rowData.push(this.prog_types[this.rowCount-1]);

           // console.log(this.colData);

          },
          (err)=>
          {
            console.log(err);
          });
},50);
}
 
            if(this.rowCount>this.prog_types.length){
              this.noMoreRows=true;
}
 //console.log(event);
  }

  doInfinite1(event){
    console.log(event);
    console.log("bello");
  }
 goToDetails(i){
   //console.log('yoyoyoyo');
   this.navCtrl.push(ProgramDetailsPage,{
     programid:i
   })
 }
}




