import { Component} from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { Vibration } from 'ionic-native';

//providers
import { DataService } from '../../../providers/DataService';
import { LocalStorageService } from '../../../providers/LocalStorageService';

//pages
import { RealRemotePage } from '../real-remote/real-remote'

@Component({
  selector: 'page-remote',
  templateUrl: 'remote.page.html'
})
export class RemotePage {
  constructor(public navCtrl: NavController,private alertCtrl:AlertController, public navParams: NavParams,private ds:DataService,private local:LocalStorageService) {

  }
data;
  ionViewDidLoad() {
    console.log('ionViewDidLoad RemotePage');
  }
    ionViewWillEnter() {

    this.ds.getIrAppliances(this.local.getCurrentHome(),
      (data)=>{
          console.log(data);
          this.data = data;
      },
      (error)=>{

      });
      
    
  }
  goToRemote(i){
    console.log(i);
    if(i.paired_sb=='null'||i.paired_sb=='undefined'||i.paired_sb==null){

        this.doRadio(i);
    }
    else{
       this.navCtrl.push(RealRemotePage,{
              codeset:i.remote_codeset,
              device_id:i.paired_sb.device_id,
              sb_id:i.paired_sb._id,
              is_stb:i.is_stb
      })
    }
     
  }
  data2;
  public doRadio(i) {
    let alert = this.alertCtrl.create();
   // alert = this.alertCtrl.create();
    alert.setTitle('Select Paired Switchboard to continue');
    this.ds.getSboards(this.local.getCurrentHome(),
    (data)=>{
              this.data2=data;
              for(let i of this.data2){
                for(let j of i.devices){
                     alert.addInput({type: 'radio', label: j.name+' in '+i.room_name, 
                            value: JSON.stringify(
                              {
                                "sb":j._id,
                                "room":i.room_id
                              }
                              )});
                }
              }
           
              alert.addButton('Cancel');
              alert.addButton({
                text: 'Select',
                handler: data => {
                  alert.dismiss();
                  i.paired_sb=[];
                  i.paired_sb.device_id=data._id;
                      let obj = {
                            "id":i._id,
                            "room_id":JSON.parse(data).room,
                            "paired_sb":JSON.parse(data).sb
                      };
                  this.ds.postEditIrApp(obj,
                  (data)=>{
                        this.goToRemote(i);
                  },
                  (error)=>{
                      console.log(error);
                  })
                  
                }
              });

              alert.present();
            },
            (error)=>{
                console.log(error);
            })

  }

}
