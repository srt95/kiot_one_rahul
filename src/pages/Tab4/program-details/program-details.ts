import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//providers
import { DataService } from '../../../providers/DataService';

@Component({
  selector: 'page-program-details',
  templateUrl: 'program-details.html'
})
export class ProgramDetailsPage {
programid;programInfo={"cast":[]};cast=[];director={};
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataService) {
    this.programid = this.navParams.get('programid');
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad ProgramDetailsPage');
    this.ds.getProgramDetails(this.programid,
      (data)=>{
        this.programInfo = data[0].program;
          console.log(data);
          for(let i of this.programInfo.cast){
                if(i.role=="cast") this.cast.push(i);
                if(i.role=="Director") this.director = i;
          }
      },
      (error)=>{
        
      })
  }

}
