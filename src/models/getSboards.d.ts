export class getSboards{
    _id:string;
    count:number;
    room_id:string;
    room_name:string;
    devices:devices[];
    switches:switches[];
    active_count:number;
}

interface devices{
    is_online:boolean;
    sensor_type:string;
    _id:string;
    device_type:string;
    device_id:string;
    name:string;
    display_color:any;
    switch_appliances:switchappliances[];
}


interface switchappliances{
    _id: string;
    switch_type:string;
    sboard_id:string;
    switch_no: number;
    is_configured:boolean;
    current_status: number;
    appliance_type:string;
    name: string;
}
interface switches{
    is_Active:boolean;
    isRegulator:boolean;
}