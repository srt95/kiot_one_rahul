export interface Rooms{
    _id:string;
    room_name:string;
    room_id:string;
    activeAppliances:number;
    totalAppliances:number;
}