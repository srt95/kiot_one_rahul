import {Rooms} from '../models/rooms.model';

export interface AppState {  
    rooms: Rooms[];
}