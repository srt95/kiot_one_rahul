import { Injectable } from '@angular/core';


@Injectable()
export class LocalStorageService{

    //default Home
    getDefaultHome(){
        return localStorage.getItem('default_home');
    }

    setDefaultHome(item:string){
        localStorage.setItem('default_home',item);
        return;
    }

    //current Home
    getCurrentHome(){
        return localStorage.getItem('current_home');
    }
    setCurrentHome(item:string){
        localStorage.setItem('current_home',item);
        return;

    }


    //Token
    getToken(){
        return localStorage.getItem('token');
    }

    setToken(token:string){
        localStorage.setItem('token',token);
    }


    //User Details
    setUser(user:any){
        localStorage.setItem('user',user);
    }
    getUser(){
        return localStorage.getItem('user');
    }

    //internetstatus
    getInternetStatus(){
        return localStorage.getItem("kiot_internet");
    }
    setInternetStatus(iStatus:string){
        localStorage.setItem("kiot_internet",iStatus);
    }
    setUserLoginObject(data:any){
        localStorage.setItem("kiot_UserLoginObject", JSON.stringify(data));
    }
    getUserLoginObject(){
        let data = localStorage.getItem("kiot_UserLoginObject");
        return JSON.parse(data);
    }


    //languagePrefs
    setEpgLangs(data:string){
        localStorage.setItem('kiot_epg_langs',data);
    }
    getEpgLangs(){
        return localStorage.getItem('kiot_epg_langs')=='undefined'?"":localStorage.getItem('kiot_epg_langs');
    }

    //genrePrefs
    setEpgTypes(data:string){
        localStorage.setItem('kiot_epg_types',data);
    }
     getEpgTypes(){
         try{
             return JSON.parse(localStorage.getItem('kiot_epg_types'));
         }catch(Err){
            return null;
         }
       
    }
    
}