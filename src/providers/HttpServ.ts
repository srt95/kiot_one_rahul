import {Injectable} from '@angular/core';
import {Http,Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {  Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from './LocalStorageService';

@Injectable()
export class HttpServ{
    token:string;
    url:string;
    baseUrl:string;
    opt1: RequestOptions;
    headers;
    myHeaders: Headers = new Headers;
 public API_ENDPOINT:string;
    constructor(private http:Http,private local:LocalStorageService){
 this.API_ENDPOINT  ='http://staging.feturtles.com:3002/';
        this.baseUrl = 'http://staging.feturtles.com:3002/api/v1/';
        this.myHeaders.append('Content-type', 'application/json');
        // this.token = this.ls.getToken();
        // console.log(this.token);
        // this.myHeaders.set('Authorization', 'Bearer '+this.token);
        // this.myHeaders.append('Content-type', 'application/json');
        // console.log(this.myHeaders);
        // this.opt1 = new RequestOptions({
        //     headers: this.myHeaders
        // });
    }

    getMe(url:any){
       this.getTokens();
       return this.http.get(this.baseUrl + url,this.opt1).map((res:Response)=>res.json());
    }
    
    postMe(url,jsonObj){
        this.getTokens();
        return this.http.post(this.baseUrl + url,jsonObj,this.opt1).map((res:Response)=>res.json());
    }
    putMe(url,jsonObj){
         this.getTokens();

        return this.http.put(this.baseUrl + url,jsonObj,this.opt1).map((res:Response)=>res.json());

    }
    getTokens(){
        let token = this.local.getToken();
        this.myHeaders.set('Authorization', 'Bearer '+token); 
        this.opt1 = new RequestOptions({
            headers: this.myHeaders
        }); 
    }
    /*testInternet(){
        return this.http.get(this.baseUrl+"homes/myhomes")
            .timeout(3000);
                
    }*/


 /**
  * This method for User Login with valid credentials
  */
    public userLogin(data: any) {

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        let loginURL = this.API_ENDPOINT + 'user/authenticate';
        return this.http.post(loginURL, data, { headers: this.headers }).map((response: Response) => response.json());
    }
        // http://staging.feturtles.com:3002/api/v1/sboards/addswitch

testInternet(){
    this.http.get(' http://staging.feturtles.com:3002/ping')
        .subscribe(data=>this.local.setInternetStatus('online'),
        error=>{
            if(error.status==0)
            this.local.setInternetStatus('offline');
        })
        
}


}
