import { Injectable, Component, NgZone } from '@angular/core';
import { HttpServ } from './HttpServ';
import { Subject } from 'rxjs';
import { PouchService } from './PouchService';
import { LocalStorageService } from './LocalStorageService';
import { LoadingService } from './LoadingService';

@Injectable()
export class DataService {

    constructor(private ht: HttpServ, private zone: NgZone, private ps: PouchService, private local: LocalStorageService, private load: LoadingService) {

        // this.notLoggedIn=false;
        // let logsub = (loggedin)=>{
        //     if (!loggedin) {
        //         // nav.setRoot(LoginPage);
        //     }
        // }
        // authTracker.loginNotifier.subscribe(logsub);
        // this.ps.initDB('KIOT');
    }

    tryRelogin(successCallback, errorCallBack) {
        let userData = this.local.getUserLoginObject();
        // let navCtrl:NavController;
        if (userData) {
            this.ht.userLogin(userData).subscribe(data => {
                if (!data.token || data.token == undefined) {

                }
                // this.token = data.token;
                this.local.setToken(data.token);
                this.local.setUser(JSON.stringify(data.user));
                return successCallback();
            }, error => {
                // this.notLoggedIn= true;
                this.local.setToken(null);
                window.location.reload();
                // this.appCtrl.getRootNav().push(LoginPage);
                // HelperPage.changePage(LoginPage);
                // navCtrl.push(LoginPage);
                // return errorCallBack();
            })
        }



    }

    getDataFromDb(url1, url2, prefix, successCallBack, errorCallBack) {
        if (this.local.getInternetStatus() == "online") {
            this.getData(url1 + url2,
                (data) => {
                    console.log(data);
                    this.postDataToDb(url2, prefix, data)
                        .then(
                        data2 => {
                            console.log(data2);
                            successCallBack(data);
                        })
                        .catch(
                        err => {
                            errorCallBack();
                            console.log("an error is occuring");
                            console.log(err);
                        }
                        )

                }, error => {
                    console.log(error);
                });
            console.log("I am passed");
        }

        else {
            this.ps.getADoc(prefix, url2).then(
                (data) => {
                    successCallBack(data);
                },
                (error) => {
                    errorCallBack();
                });
        }
    }
    postDataToDb(url, prefix, data) {
        let promiseArray = [];
        if (Array.isArray(data) == true) {
            this.ps.removeAll(prefix, url)
                .then(data5 => {
                    console.log(data5);
                    for (let i of data) {
                        promiseArray.push(this.ps.add(prefix, url, i, false));
                    }
                }
                ).catch(
                err => { console.log(err); return Promise.reject(err); }
                );

            /*   let dataArray=[{dummy:"value"}];
                  dataArray.splice(0,1);
             
           let j=0;
           for(let i of data){
               //this.ps.update(prefix,url,i);
               console.log('loop in postdatatodb');
                   this.ps.check(prefix,url,i)
                   .then( data2=>{
                         //  console.log(data2);
                         i._rev=data2._rev;
                         // data2=i;
                          //dataArray.push(data2);
                           // console.log(i._rev);
                          // dataArray[j]=i;
                        //  dataArray.push(i);
                          promiseArray.push(this.ps.add(prefix,url,i,data2._rev));
                        //  this.ps.add(prefix,url,i,data2._rev).then(data=>console.log(data));
                   }).catch(err=>{
                           if(err.name === 'not_found'){
                              // dataArray.push(i);
                              // dataArray[j]=i;
                             promiseArray.push(this.ps.add(prefix,url,i,false)); 
                            
                           }
                   })
                   
               //promiseArray.push (this.ps.update(prefix,url,i));
               j++;
              // console.log(data);
           }*/
            return Promise.all(promiseArray);
        }
        else {
            return Promise.resolve('okay');
        }
        /*.then(
            (data)=>{
                successCallBack(data);
            },
            (error)=>{
                errorCallBack(error);
            });*/
        //return Promise.all(promiseArray);
    }

    getData(url, sucessCallBack, errorCallBack, showLoadingBar);
    getData(url, successCallBack, errorCallBack);
    getData(url, successCallBack, errorCallBack, showLoadingBar?: boolean) {
        if (showLoadingBar != false) this.load.showLoading('loading');
        this.ht.getMe(url)
            .subscribe((data33) => {
                if (showLoadingBar != false) this.load.dismiss();
                console.log(data33);
                successCallBack(data33);
                this.local.setInternetStatus("online");

            },
            (error) => {
                if (error.status == 0) {
                    this.local.setInternetStatus("offline");
                    if (showLoadingBar != false) this.load.dismiss();
                }
                else if (error.status == 401) {
                    this.tryRelogin(() => {
                        // successCallback
                        this.ht.getMe(url)
                            .subscribe((data) => {
                                this.load.dismiss();
                                this.local.setInternetStatus("online");
                                return successCallBack(data);

                            });
                    }, () => {
                        //errorCallBack
                        if (showLoadingBar != false) this.load.dismiss();
                        return errorCallBack(error);
                    });

                }
                else {
                    this.load.dismiss();
                    return errorCallBack(error);
                }
            });
    }


    postData(url, jsonObj, successCallBack, errorCallBack) {
        this.load.showLoading('loading');
        this.ht.postMe(url, jsonObj)
            .subscribe((data) => {
                this.load.dismiss();
                this.local.setInternetStatus("online");
                return successCallBack(data);

            }, (error) => {

                if (error.status == 0) {
                    this.local.setInternetStatus("offline");
                    this.load.dismiss();
                }
                else if (error.status == 401) {
                    this.tryRelogin(() => {
                        // successCallback
                        this.ht.postMe(url, jsonObj)
                            .subscribe((data) => {
                                this.load.dismiss();
                                this.local.setInternetStatus("online");
                                return successCallBack(data);

                            });
                    }, () => {

                        //errorCallBack
                        this.load.dismiss();
                        return errorCallBack(error);
                    });

                }
                else {
                    this.load.dismiss();
                    return errorCallBack(error);
                }

            });
    }
    putData(url, jsonObj, successCallBack, errorCallBack) {
        console.log(jsonObj);
        this.load.showLoading('loading');
        this.ht.putMe(url, jsonObj)
            .subscribe((data) => {
                this.load.dismiss();
                successCallBack(data);
                this.local.setInternetStatus("online");
            }, (error) => {
                if (error.status == 0) {
                    this.local.setInternetStatus("offline");
                    this.load.dismiss();
                }
                else if (error.status == 401) {
                    this.tryRelogin(() => {
                        // successCallback
                        this.ht.putMe(url, jsonObj)
                            .subscribe((data) => {
                                this.load.dismiss();
                                this.local.setInternetStatus("online");
                                return successCallBack(data);

                            });
                    }, () => {
                        //errorCallBack
                        this.load.dismiss();
                        return errorCallBack(error);
                    });

                }
                else {
                    this.load.dismiss();
                    return errorCallBack(error);
                }
            });
    }




    //get REQUESTS go here
    getUserHomes(successCallBack, errorCallBack) {
        let def = localStorage.getItem('default_home');
        this.getData('homes/myhomes', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getUserRooms(successCallBack, errorCallBack) {
        let def = localStorage.getItem('default_home');
        this.getData('rooms/allmyrooms', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getHomesRooms(str, successCallBack, errorCallBack) {
        let def = localStorage.getItem('default_home');
        this.getData('rooms/myrooms?home=' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getJoinRequests(successCallBack, errorCallBack) {
        this.getData('homes/my_home_join_requests', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSearchHome(str, successCallBack, errorCallBack) {
        this.getData('homes/search_home_basic?home=' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getRooms(str, successCallBack, errorCallBack) {
        this.getDataFromDb('rooms/home_rooms?home=', str, 'room', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSboards(str, successCallBack, errorCallBack) {
        this.getData('sboards/mysboards?home=' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSboardsFromDb(str, successCallBack, errorCallBack) {
        this.getDataFromDb('sboards/mysboards?home=', str, 'room', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSwitches(str, successCallBack, errorCallBack) {
        this.getData('sboards/mysboards?home=' + str, (data) => {

            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getSwitchesInRoom(str, successCallBack, errorCallBack) {
        this.getDataFromDb('sboards/byroom?room=', str, 'switches', (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getIrAppliances(str, successCallBack, errorCallBack) {
        this.getData('ir_appliances/myapps?home=' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getBrands(str, successCallBack, errorCallBack) {
        this.getData('appliances/type/' + str, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getTestCodeSets(str1, str2, successCallBack, errorCallBack) {
        this.getData('appliances/test_codeset?brand=' + str1 + '&type=' + str2, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }
    getFullCodeSets(str1, successCallBack, errorCallBack) {
        this.getData('codesets?id=' + str1,
            (data) => {
                // this.ps.initDB('KIOT')
                this.ps.downloadRemote(data[0]);
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    getRemoteFromDb(str1, successCallBack, errorCallBack) {
        console.log(str1);
        this.ps.getADoc('codeset', str1)
            .then(data => {
                // this.ps.initDB('KIOT')
                if (data) {
                    if (data.length == 0) {
                        console.log('yoyo');
                        this.getFullCodeSets(str1,
                            (data) => {
                                return successCallBack(data);
                            },
                            (error) => {
                                return errorCallBack(error);
                            })
                    }
                    else {
                        return successCallBack(data);
                    }
                }

            }).catch(
            error => {
                return errorCallBack(error);
            });
    }
    getGeneralAppliances(successCallBack, errorCallBack) {
        this.getData('sboards/general_appliances',
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    getChannelGroupData(successCallBack, errorCallBack) {
        this.load.showLoading('yo');
        this.ps.getADoc('channel', '')
            .then(
            data => {

                if (data.length != 4) {                    
                alert(data[0]);
                alert(data[1]);
                alert(data[2]);
                    console.log(data);
                    this.getData('epg/channelMeta?limit=1000&offset=2',
                        (data) => {
                            this.load.dismiss();
                            let channelData = [];
                            let promiseArray = [];
                            let dataToSendAsArray = [];
                            promiseArray.push(this.ps.add('channel', 'group', { channelGroups: data.channelGroups }, false));
                            promiseArray.push(this.ps.add('channel', 'Langs', data.channelLangs, false));
                            promiseArray.push(this.ps.add('channel', 'Types', data.channelTypes, false));
                            for (let j of data.channels) {
                                channelData[j.id] = j;
                            }
                            promiseArray.push(this.ps.add('channel', 'names', { channels: channelData }, false));
                            // console.log(channelData);

                            Promise.all(promiseArray).then(
                                doc => {
                                    this.load.dismiss();
                                    dataToSendAsArray.push(data.channelLangs);
                                    dataToSendAsArray.push(data.channelTypes);
                                    dataToSendAsArray.push({ channelGroups: data.channelGroups });
                                    dataToSendAsArray.push({ channels: channelData });
                                    console.log("Suceess 1");
                                    alert('success 1');
                                    return successCallBack(dataToSendAsArray);
                                }
                            ).catch(
                                err => {
                                    alert(err);
                                    console.log("Error 2");
                                    this.load.dismiss();
                                    return errorCallBack(err);
                                })

                        }, (error) => {
                            alert(error);
                            console.log("Error 1");
                            this.load.dismiss();
                            return errorCallBack(error);
                        }, false);
                }
                else {
                    alert("success2");
                    this.load.dismiss();
                    console.log("Success 2");
                    return successCallBack(data);
                }
            }

            ).catch(
            err => {

                if (err.name == "not_found") {
                    this.getData('epg/channelGroups?limit=10&offset=2',
                        (data) => {
                            this.load.dismiss();
                            return successCallBack(data);
                        }, (error) => {
                            this.load.dismiss();
                            return errorCallBack(error);
                        });
                }
                alert(err);
                console.log("Error 4");
                this.load.dismiss();
                return errorCallBack(err);

            }
            );
    }
    getPrograms(limit, offset, languages, types, showLoader, successCallBack, errorCallBack) {
        this.getData('epg/programs?limit=' + limit + '&offset=' + offset + '&languages=' + languages + '&programTypes=' + types,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            }, showLoader);
    }
    getProgramDetails(programid, successCallBack, errorCallBack) {
        this.getData('epg/programs?programid=' + programid,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }




    //post REQUESTS go here
    postEmailForgotPassword(obj, successCallBack, errorCallBack) {
        this.putData('user/authenticate/forgot_password', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        })
    }
    postUpdatePassword(obj, successCallBack, errorCallBack) {
        this.postData('users/update_password', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postCheckExisting(obj, successCallBack, errorCallBack) {
        this.postData('homes/check_existing', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postCreateHome(obj, successCallBack, errorCallBack) {
        this.postData('homes/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditHome(obj, successCallBack, errorCallBack) {
        this.postData('homes/myhomes', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postDeleteHome(obj, successCallBack, errorCallBack) {
        this.postData('homes/myhomes/delete', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postCreateWifiRouter(obj, successCallBack, errorCallBack) {
        this.postData('homes/wifi', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditWifiRouter(obj, successCallBack, errorCallBack) {
        this.postData('homes/wifi', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postDeleteWifiRouter(obj, successCallBack, errorCallBack) {
        this.postData('homes/wifi', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postJoinRequest(obj, successCallBack, errorCallBack) {
        this.postData('homes/request_join', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postApproveJoinRequest(obj, successCallBack, errorCallBack) {
        this.postData('homes/my_home_join_requests', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postRemoveUser(obj, successCallBack, errorCallBack) {
        this.postData('homes/remove_user', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postCreateRoom(obj, successCallBack, errorCallBack) {
        this.postData('rooms/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postEditRoom(obj, successCallBack, errorCallBack) {
        this.postData('rooms/myroom', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postDeleteRoom(obj, successCallBack, errorCallBack) {
        this.postData('rooms/myroom/delete', obj,
            (data) => {
                // this.ps.removeADoc(room,)
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postCreateSboard(obj, successCallBack, errorCallBack) {
        this.postData('sboards/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postEditSwitchBoard(obj, successCallBack, errorCallBack) {
        this.postData('sboards/sboard', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postDeleteSwitchBoard(obj, successCallBack, errorCallBack) {
        this.postData('sboards/sboard/delete', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postCreateSwitch(obj, successCallBack, errorCallBack) {
        this.postData('sboards/addswitch', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postEditSwitch(obj, successCallBack, errorCallBack) {
        this.postData('sboards/switch', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            });
    }
    postCreateIrApp(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/new', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postEditIrApp(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/myapps', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postDeleteIrApp(obj, successCallBack, errorCallBack) {
        this.postData('ir_appliances/myapps/delete', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postTriggerIr(obj, successCallBack, errorCallBack) {
        this.postData('deviceevents/ircustom', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postTriggerSwitch(obj, successCallBack, errorCallBack) {
        this.postData('deviceevents/switch', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }
    postTurnOffRoom(obj, successCallBack, errorCallBack) {
        this.postData('deviceevents/turnoff_room', obj,
            (data) => {
                return successCallBack(data);
            }, (error) => {
                return errorCallBack(error);
            })
    }





    //put REQUESTS go here
    putEditProfile(obj, successCallBack, errorCallBack) {
        console.log(obj);
        this.putData('users/me', obj, (data) => {
            return successCallBack(data);
        }, (error) => {
            return errorCallBack(error);
        });
    }


}