import{ Injectable } from '@angular/core';

@Injectable()
export class Helper{

    constructor(){

    }

    isEmpty(param){
            if(Array.isArray(param)){
                if(param.length == 0){
                    return true;
                }
                else return false;
            }
            else {
                if(param==""||param=="undefined"||param=="null"||param==null||JSON.stringify(param)=="[]"){
                    return true;
                }
                else return false;
            }

    }
//helper for switch appliance ON duration
    findDuration(obj,lastKnownStatus){
        let current = false;
        let startTime;
        let totalHrsDuration=0;
        let totalMinsDuration=0;
        let progressbarDuration=0;
        let mins;
        console.log(obj);
        if(obj.length==0){
            if(lastKnownStatus==1){
                var now = new Date(),
                    then = new Date(
                        now.getFullYear(),
                        now.getMonth(),
                        now.getDate(),
                        0,0,0);
                    totalMinsDuration = this.findMinsInterval(then,now);
            }

        }
else{
        if(obj[0].event_type==0){
            let mid = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate(),0,0,0);
            totalMinsDuration = this.findMinsInterval(mid,obj[0].timestamp);
        }
        for(let i of obj){
            if(i.event_type==1){
                if(!current){
                    current = true;
                    startTime = i.timestamp;
                }
            }
            if(i.event_type==0){
                if(current){
                    current = false;
                  //  totalHrsDuration+= this.findHrsInterval(startTime,i.timestamp);
                    totalMinsDuration += this.findMinsInterval(startTime,i.timestamp);
                }
            }
        }
        if(current){
          //  totalHrsDuration+= Math.floor(this.findHrsInterval(startTime,Date.now()));
            totalMinsDuration += Math.floor(this.findMinsInterval(startTime,Date.now()));
        }
}



        if(totalMinsDuration<60){mins=totalMinsDuration;}
        else{
            totalHrsDuration = Math.floor(totalMinsDuration/60);
            mins = (totalMinsDuration%60);
        }
         progressbarDuration = ((totalMinsDuration)/(24*60))*100;
        return [Math.floor(totalHrsDuration),mins?Math.floor(mins):0,progressbarDuration];
    }
    findHrsInterval(startTime,endTime){
        let diff,hrs;
        diff = endTime - startTime;
        hrs = diff/1000/60/60; 
        return hrs;
    }
    findMinsInterval(startTime,endTime){
        let diff,mins;
        diff = endTime - startTime;
        mins = diff/1000/60;
        //console.log(mins);
        return mins;
    }
    


}