import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { Rooms } from '../models/rooms.model';

@Injectable()
export class RoomActions{
        static ADD_ROOM = 'ADD_ROOM';
        addRoom(room:Rooms):Action{
            return{
                    type: RoomActions.ADD_ROOM,
                    payload: room
            }
        }    
        static UPDATE_ROOM = 'UPDATE_ROOM';
        updateRoom(room:Rooms):Action{
            return{
                    type: RoomActions.UPDATE_ROOM,
                    payload: room
            }
        }
        static DELETE_ROOM = 'DELETE_ROOM';
        deleteRoom(room:Rooms):Action{
            return{
                    type: RoomActions.DELETE_ROOM,
                    payload: room
            }
        }
        static LOAD_ROOMS_SUCCESS = 'LOAD_ROOMS_SUCCESS';  
        loadRoomsSuccess(Rooms: Rooms[]): Action {  
            return {
                type: RoomActions.LOAD_ROOMS_SUCCESS,
                payload: Rooms
            }
        }

        static ADD_UPDATE_ROOM_SUCCESS = 'ADD_UPDATE_ROOM_SUCCESS';  
        addUpdateRoomSuccess(Room: Rooms): Action {  
            return {
                type: RoomActions.ADD_UPDATE_ROOM_SUCCESS,
                payload: Room
            }
        }

        static DELETE_ROOM_SUCCESS = 'DELETE_ROOM_SUCCESS';  
        deleteRoomSuccess(id: string): Action {  
            return {
                type: RoomActions.DELETE_ROOM_SUCCESS,
                payload: id
            }
        }  
}